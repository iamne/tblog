<?php
/**
 * Main Seotoaster blog class
 *
 * @author Eugene I. Nezhuta <eugene.nezhuta@gmail.com>
 */
class Tblog extends Tools_Plugins_Abstract {

    /**
     * Option to indicate blog index page
     *
     */
    const OPTION_PAGE_INDEX   = 'option_tblog_index_page';

    /**
     * Option to indicate regular blog pages
     *
     */
    const OPTION_PAGE_DEFAULT = 'option_tblog_regular_page';

    /**
     * Tblog route name
     *
     */
    const ROUTE_NAME          = 'tblogRoute';

    /**
     * Tblog index page route
     *
     */
    const ROUTE_INDEX_NAME    = 'tblogIndexRoute';

    /**
     * Default blog index page url
     *
     */
    const DEAFULT_INDEX_URL   = 'd36637530217f0d6b0547a6e0e970d60';

    /**
     * Postfix for the screens view scripts
     */
    const VIEWS_POSTFIX       = '.screen.phtml';

    /**
     * Zend_Layout instance
     *
     * @var null|Zend_Layout
     */
    private $_layout         = null;

    /**
     * Tblog settings
     *
     * @var array
     */
    private $_settings       = array();


    protected function _init() {
        $this->_layout = new Zend_Layout();
        $this->_layout->setLayoutPath(__DIR__ . '/system/views/');
        if(($scriptPaths = Zend_Layout::getMvcInstance()->getView()->getScriptPaths()) !== false) {
            $this->_view->setScriptPath($scriptPaths);
        }
        $this->_view->addScriptPath(__DIR__ . '/system/views/');
        $this->_settings = Tblog_Models_Mapper_SettingsMapper::getInstance()->fetchSettings();
    }

    public function postsAction() {
        $this->_show(null, false);
    }

    public function dashboardAction() {
        $this->_show();
    }

    /**
     * Add Tblog index and regular pages routes
     *
     */
    public function beforeRouter() {
        if(!isset($this->_settings['route']) || !$this->_settings['route']) {
            return;
        }

        $router = Zend_Controller_Front::getInstance()->getRouter();

        //add blog page route
        $router->addRoute(self::ROUTE_NAME,
            new Zend_Controller_Router_Route($this->_settings['route'] . '/:page', array(
                'controller' => 'index',
                'action'     => 'index',
                'page'       => self::DEAFULT_INDEX_URL
            ))
        );

        // add blog page index route
        $router->addRoute(self::ROUTE_INDEX_NAME,
            new Zend_Controller_Router_Route($this->_settings['route'], array(
                'controller' => 'index',
                'action'     => 'index',
                'page'       => self::DEAFULT_INDEX_URL
            ))
        );
    }

    /**
     * Before controller hook. Resolve routes and add extra content to the layout
     *
     */
    public function beforeController() {
        $this->_resolveRoutes();

        if(Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_ADMINPANEL)) {
            $this->_injectContent($this->_view->render('extra.layout.phtml'));
        }
    }

    /**
     * Resolve tblog routes
     *
     * @return bool
     */
    private function _resolveRoutes() {
        $routeName     = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
        $pageMapper    = Application_Model_Mappers_PageMapper::getInstance();
        $page          = $pageMapper->findByUrl($this->_request->getParam('page'));

        if($routeName == self::ROUTE_NAME) {
            if(!$page instanceof Application_Model_Models_Page) {
                return false;
            }
            if($page->getExtraOption(self::OPTION_PAGE_INDEX)) {
                $this->_redirector->gotoUrl($this->_websiteUrl . $this->_settings['route'] . '/');
            }
            $this->_view->blogMenuPosition = 'replace';
        } else if ($routeName == self::ROUTE_INDEX_NAME) {
            // if on blog index page, set hint for a view to show blog menu at first place
            $this->_view->blogMenuPosition = 'ontop';

            $page = $pageMapper->fetchByOption(self::OPTION_PAGE_INDEX, true);
            $this->_request->setParam('page', $page->getUrl());
        } else {
            if(!$page instanceof Application_Model_Models_Page) {
                return false;
            }
            if($page->getExtraOption(self::OPTION_PAGE_DEFAULT)) {
                $this->_redirector->gotoUrl($this->_websiteUrl . $this->_settings['route'] . '/' . $page->getUrl());
            }
            if($page->getExtraOption(self::OPTION_PAGE_INDEX)) {
                $this->_redirector->gotoUrl($this->_websiteUrl . $this->_settings['route'] . '/');
            }
        }
        return true;
    }

    /**
     * Render a proper view script
     *
     * If $screenViewScript not passed, generates view script file name automatically using the action name and VIEWS_POSTFIX
     *
     * @param string $screenViewScript
     * @param boolean $useLayout
     */
    private function _show($screenViewScript = '', $useLayout = true) {
        if(!$screenViewScript) {
            $trace  = debug_backtrace(false);
            $screenViewScript = str_ireplace('Action', self::VIEWS_POSTFIX, $trace[1]['function']);
        }
        if(!$useLayout) {
            echo $this->_view->render($screenViewScript);
            return;
        }
        $this->_layout->content = $this->_view->render($screenViewScript);
        echo $this->_layout->render();
    }

}