<?php

class Api_Tblog_Media extends Api_Service_Abstract {

    const LIST_MODE_IMAGES    = 'images';

    const LIST_MODE_FILES     = 'files';

    const LIST_MODE_All       = 'all';

    const ALLOWED_IMAGES      = '{jpg,png,gif,bmp,jpeg}';

    const ALLOWED_FILES       = '{zip,rar,tar,tar.gz,7z,pdf,doc,docx,xls,xlsx,rtf}';

    const TEASER_IMAGE_PREFIX = 'tblog_teaser_';

    private $_websiteHelper = null;

    private $_uploader      = null;

    protected $_accessList   = array(
        Tools_Security_Acl::ROLE_SUPERADMIN => array('allow' => array('get', 'post', 'put', 'delete')),
        Tools_Security_Acl::ROLE_ADMIN      => array('allow' => array('get', 'post', 'put', 'delete')),
        Tools_Security_Acl::ROLE_GUEST      => array('allow' => array('get'))
    );

    public function init() {
        $this->_websiteHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('website');
    }

    public function getAction() {
        if(!$this->_request->has('folder')) {
            $this->_error('Media folder is missing.', self::REST_STATUS_BAD_REQUEST);
        }
        $folder = trim(filter_var($this->_request->getParam('folder'), FILTER_SANITIZE_STRING));

        $listMode = self::LIST_MODE_All;
        if($this->_request->has('lmode')) {
            $listMode = trim(filter_var($this->_request->getParam('lmode'), FILTER_SANITIZE_STRING));
            if(!$this->_validateListMode($listMode)) {
                $this->_error('List mode is invalid.', self::REST_STATUS_BAD_REQUEST);
            }
        }

        $mediaPath = $this->_websiteHelper->getPath() . $this->_websiteHelper->getMedia() . $folder ;
        $media     = array();
        switch($listMode) {
            case self::LIST_MODE_All:
                $media = array_merge(
                    array('images' => glob($mediaPath . '/small/*.' . self::ALLOWED_IMAGES, GLOB_BRACE)),
                    array('files' => glob($mediaPath . '/*.' . self::ALLOWED_FILES, GLOB_BRACE))
                );
            break;
            case self::LIST_MODE_IMAGES:
                $media = glob($mediaPath . '/small/*.' . self::ALLOWED_IMAGES, GLOB_BRACE);
            break;
            case self::LIST_MODE_FILES:
                $media = glob($mediaPath . '/*.' . self::ALLOWED_FILES, GLOB_BRACE);
            break;
        }
        if(!is_array($media) || empty($media)) {
            return array();
        }

        if($listMode == self::LIST_MODE_All) {
            $media['images'] = $this->_prepareData($media['images'], $folder);
            $media['files'] = $this->_prepareData($media['files'], $folder);
        } else {
            $media = $this->_prepareData($media, $folder);
        }
        return $media;
    }

    public function postAction() {
        $this->_uploader = new Zend_File_Transfer_Adapter_Http();
        $this->_uploader->clearFilters()
            ->clearValidators();

        $teaserName = self::TEASER_IMAGE_PREFIX . md5(uniqid()) . $this->_getExtensionByMime();
        $teaserPath = $this->_websiteHelper->getPath() . $this->_websiteHelper->getTmp() . $teaserName;

        $this->_uploader->addFilter('Rename', array(
            'target'    => $teaserPath,
            'overwrite' => true
        ))->receive();

        return $this->_websiteHelper->getTmp() . $teaserName;
    }

    public function putAction() {}
    public function deleteAction() {}

    private function _validateListMode($listMode) {
        return true;
    }

    private function _prepareData($media, $folder) {
        return array_map(function($file) use ($folder) {
            return $folder . DIRECTORY_SEPARATOR . pathinfo($file, PATHINFO_BASENAME);
        }, $media);
    }

    private function _getMimeType(){
        if (extension_loaded('fileinfo')) {
            return $this->_uploader->getMimeType();
        }
        $files = $this->_uploader->getFileInfo();
        if (empty($files)) {
            return false;
        }
        $file = reset($files);
        unset($files);
        if (function_exists('getimagesize')){
            $info = getimagesize($file['tmp_name']);
            return $info !== false ? $info['mime'] : false;
        }
        return false;
    }

    private function _getExtensionByMime() {
        $extension = '';

        switch ($this->_getMimeType()){
            case 'image/png' : $extension = '.png'; break;
            case 'image/jpg' :
            case 'image/jpeg': $extension = '.jpg'; break;
            case 'image/gif' : $extension = '.gif'; break;
            default          : $extension = false; break;
        }

        return $extension;
    }
}