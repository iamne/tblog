<?php

class Api_Tblog_Posts extends Api_Service_Abstract {

    protected $_mapper       = null;

    protected $_accessList   = array(
        Tools_Security_Acl::ROLE_SUPERADMIN => array('allow' => array('get', 'post', 'put', 'delete')),
        Tools_Security_Acl::ROLE_ADMIN      => array('allow' => array('get', 'post', 'put', 'delete')),
        Tools_Security_Acl::ROLE_GUEST      => array('allow' => array('get'))
    );

    public function init() {
        $this->_mapper = Tblog_Models_Mapper_PostMapper::getInstance();
    }

    public function getAction() {
        if($this->_request->has('id')) {
            $postId = filter_var($this->_request->getParam('id'), FILTER_SANITIZE_NUMBER_INT);
            if(!$postId) {
                $this->_error('Parameters passed to the request are not valid');
            }

            $post = $this->_mapper->find($postId);
            if(!$post instanceof Tblog_Models_Model_Post) {
                $this->_error('Cannot find post with such id', self::REST_STATUS_NOT_FOUND);
            }
            return $post->toArray();
        }
        $posts = $this->_mapper->fetchAll(null, array('updated_at DESC'));
        return empty($posts) ? array() : array_map(function($post) { return $post->toArray(); }, $posts);
    }

    public function postAction() {
        $postData = Zend_Json::decode($this->_request->getRawBody());
        return $this->_save($postData);
    }

    public function putAction() {
        $postData = Zend_Json::decode($this->_request->getRawBody());
        return $this->_save($postData);
    }

    public function deleteAction() {
        $postId = filter_var($this->_request->getParam('id'), FILTER_SANITIZE_NUMBER_INT);
        if(!$postId) {
            $this->_error();
        }
        $post = $this->_mapper->find($postId);
        if(!$post instanceof Tblog_Models_Model_Post) {
            $this->_error('Post not found.', self::REST_STATUS_NOT_FOUND);
        }
        $post->registerObserver(new Tblog_Tools_Watchdog_Post(array(
            'action' => Tblog_Tools_Watchdog_Post::CLEAN_ONDELETE
        )));
        return $this->_mapper->delete($post);
    }

    private function _save($postData) {
        // prepare tags
        if(isset($postData['tags']) && $postData['tags']) {
            $postData['tags'] = Tblog_Tools_Tools::uniqueTags($postData['tags']);
        }
        // process the post
        $post = new Tblog_Models_Model_Post($postData);
        $post->setUserId(Zend_Controller_Action_HelperBroker::getStaticHelper('session')->getCurrentUser()->getId());
        $post->registerObserver(new Tblog_Tools_Watchdog_Post(array(
            'action' => Tblog_Tools_Watchdog_Post::CLEAN_ONCREATE
        )));
        try {
            $post = $this->_mapper->save($post);
            return $post->toArray();
        } catch(Exceptions_Tblog $tbe) {
            $this->_error($tbe);
        }
    }


}