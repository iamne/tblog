<?php
class MagicSpaces_Tbloglist_Tbloglist extends Tools_MagicSpaces_Abstract {

    /**
     * Widget name that will be used in the replacement
     *
     */
    const POST_WIDGET_NAME    = 'tpost';

    /**
     * Tblog's post list order direction. Descending by default.
     *
     * @var string
     */
    protected $_orderDirection = 'DESC';

    /**
     * How many posts should be in list. Leave null to get everything.
     *
     * @var mixed integer|null
     */
    protected $_limit         = null;

    protected function _run() {
        $tmpContent     = $this->_content;
        $this->_content = $this->_getCurrentTemplateContent();
        $spaceContent   = $this->_parse();
        $this->_content = $tmpContent;
        $content        = '';

        if(!$spaceContent) {
            $spaceContent = $this->_parse();
        }

        $this->_parseParams();
        $posts = Tblog_Models_Mapper_PostMapper::getInstance()->fetchAll(null, array('created_at ' . $this->_orderDirection), $this->_limit, 0);

        if(!is_array($posts) || empty($posts)) {
            return Zend_Controller_Action_HelperBroker::getStaticHelper('language')->translate('You do not have posts yet.');
        }

        $this->_spaceContent = $spaceContent;
        foreach($posts as $post) {
            $content .= preg_replace('~{\$' . self::POST_WIDGET_NAME . ':(.+)}~uU', '{$' . self::POST_WIDGET_NAME . ':' . $post->getPageId() . ':$1}', $spaceContent);
        }
        $parser = new Tools_Content_Parser($content, array());
        return  $parser->parseSimple();
    }

    /**
     * Parse magic space parameters $_params and init appropriate properties
     *
     */
    private function _parseParams() {
        if(!is_array($this->_params)) {
            return false;
        }
        foreach($this->_params as $param) {
            $param = strtolower($param);
            if(is_string($param) && ($param == 'asc' || $param == 'desc')) {
                $this->_orderDirection = $param;
                continue;
            }
            $this->_limit = intval($param);
        }
    }

    /**
     * Get the html (not parsed) version of the current template
     *
     * @return bool|string
     */
    private function _getCurrentTemplateContent() {
        $page    = Application_Model_Mappers_PageMapper::getInstance()->find($this->_toasterData['id']);
        $tempate = Application_Model_Mappers_TemplateMapper::getInstance()->find($page->getTemplateId());
        if(!$tempate instanceof Application_Model_Models_Template) {
            return false;
        }
        return $tempate->getContent();
    }
}