<?php

class Tblog_Models_DbTable_Post extends Zend_Db_Table_Abstract {

    protected $_name = 'plugin_tblog_post';

    protected $_dependentTables = array(
        'Tblog_Models_DbTable_PostHasTag'
    );

    public function find($id) {
        return $this->fetchAll($this->getAdapter()->quoteInto('p.id=?', $id));
    }

    public function fetchAll($where = null, $order = array()) {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('p' => 'plugin_tblog_post'))
            ->joinLeft(array('pht' => 'plugin_tblog_post_has_tag'), 'p.id = pht.post_id', array())
            ->joinLeft(array('t' => 'plugin_tblog_tag'), 't.id = pht.tag_id', array('tags' => new Zend_Db_Expr('GROUP_CONCAT(t.name)')))
            ->group('p.id')
            ->order($order);
        if($where) {
            $select->where(preg_replace('~^id~', 'p.id', $where));
        }
        return parent::fetchAll($select);
    }

}