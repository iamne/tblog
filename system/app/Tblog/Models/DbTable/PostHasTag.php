<?php

class Tblog_Models_DbTable_PostHasTag extends Zend_Db_Table_Abstract {

    protected $_name = 'plugin_tblog_post_has_tag';

    protected $_referenceMap = array(
        'Posts' => array(
            'columns'		=> 'post_id',
            'refTableClass'	=> 'Tblog_Models_DbTable_Post',
            'refColumns'	=> 'id'
        ),
        'Tags' => array(
            'columns'		=> 'tag_id',
            'refTableClass'	=> 'Tblog_Models_DbTable_Tag',
            'refColumns'	=> 'id'
        )
    );

}