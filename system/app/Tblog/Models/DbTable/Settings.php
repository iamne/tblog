<?php

class Tblog_Models_DbTable_Settings extends Zend_Db_Table_Abstract {

    protected $_name = 'plugin_tblog_settings';

    public function selectSettings() {
        return $this->getAdapter()->fetchPairs($this->select());
    }

}