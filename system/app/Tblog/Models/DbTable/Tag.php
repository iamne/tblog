<?php

class Tblog_Models_DbTable_Tag extends Zend_Db_Table_Abstract {

    protected $_name = 'plugin_tblog_tag';

    protected $_dependentTables = array(
        'Tblog_Models_DbTable_PostHasTag'
    );

}