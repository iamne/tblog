<?php

class Tblog_Models_Mapper_PostMapper extends Application_Model_Mappers_Abstract {

    protected $_dbTable = 'Tblog_Models_DbTable_Post';

    protected $_model   = 'Tblog_Models_Model_Post';

    /**
     * Save post to the system
     *
     * @param $post
     * @return Tblog_Models_Model_Post
     * @throws Exceptions_TblogException
     */
    public function save($post) {
        if(!$post instanceof Tblog_Models_Model_Post) {
            throw new Exceptions_TblogException('Cannot save post. Value passed to mapper should be Tblog_Models_Model_Post instance');
        }

        $data = array(
            'title'        => $post->getTitle(),
            'teaser'       => $post->getTeaser(),
            'teaser_image' => $post->getTeaserImage(),
            'content'      => $post->getContent(),
            'user_id'      => $post->getUserId(),
            'page_id'      => $post->getPageId()
        );

        if($id = $post->getId()) {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        } else {
            $data['created_at'] = date(Tools_System_Tools::DATE_MYSQL);
            $postId             = $this->getDbTable()->insert($data);
            if(!$postId) {
                throw new Exceptions_TblogException('Cannot save the post.');
            }
            $post->setId($postId);
        }

        // saving post tags
        if(($tags = $post->getTags()) !== null) {
            $this->_savePostTags($post->getId(), $tags);
        }

        $post->notifyObservers();
        return $post;
    }

    /**
     * Find post by its page id
     *
     * @param integer $id
     * @return null|Tblog_Models_Model_Post
     */
    public function findByPageId($id) {
        $post = $this->_findWhere($this->getDbTable()->getAdapter()->quoteInto('page_id = ?', $id));
        return (!$post instanceof $this->_model) ? null : $post;
    }

    public function find($id) {
        $post = parent::find($id);
        if(!$post instanceof $this->_model) {
            return null;
        }
        $post->setTags(Tblog_Models_Mapper_TagMapper::getInstance()->findByPostId($post->getId()));
        return $post;
    }

    /**
     * Delete post from the system
     *
     * @param Tblog_Models_Model_Post $post
     * @return mixed
     */
    public function delete($post) {
        $where  = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $post->getId());
        $result = $this->getDbTable()->delete($where);
        $post->notifyObservers();
        return $result;
    }

    /**
     * Save tags for post
     *
     * @param integer $postId
     * @param array $tags
     */
    private function _savePostTags($postId, $tags) {
        $tagMapper         = Tblog_Models_Mapper_TagMapper::getInstance();
        $postHasTagDbTable = new Tblog_Models_DbTable_PostHasTag();

        $postHasTagDbTable->getAdapter()->beginTransaction();
        $postHasTagDbTable->delete($postHasTagDbTable->getAdapter()->quoteInto('post_id = ?', $postId));

        foreach($tags as $tag) {
            $tagName = strtolower($tag);
            $tag     = $tagMapper->findByName($tagName);
            if(!$tag instanceof Tblog_Models_Model_Tag) {
                $tag = $tagMapper->save(new Tblog_Models_Model_Tag(array('name' => $tagName)));
            }
            $postHasTagDbTable->insert(array(
                'post_id' => $postId,
                'tag_id'  => $tag->getId()
            ));
        }

        $postHasTagDbTable->getAdapter()->commit();
    }
}