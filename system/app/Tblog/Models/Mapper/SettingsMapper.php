<?php

class Tblog_Models_Mapper_SettingsMapper extends Application_Model_Mappers_Abstract {

    protected $_dbTable = 'Tblog_Models_DbTable_Settings';

    public function save($settings) {
        if(!is_array($settings) || empty($settings)) {
            throw new Exceptions_TblogException('Cannot save settings. Wrong value passed.');
        }
        foreach($settings as $name => $value) {
            $data  = array('value' => $value);
            $value = $this->fetchValue($name);

            if($value !== null) {
                $this->getDbTable()->update($data, $this->getDbTable()->getAdapter()->quoteInto('name=?', $name));
            } else {
                $data['name'] = $name;
                $this->getDbTable()->insert($data);
            }
        }
        return $settings;
    }

    public function fetchSettings() {
        return $this->getDbTable()->selectSettings();
    }

    public function fetchValue($name) {
        if (!$name) {
            return null;
        }
        $row = $this->getDbTable()->find($name);
        if ($row = $row->current()){
            return $row->value;
        }
        return null;
    }

}