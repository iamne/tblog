<?php
class Tblog_Models_Mapper_TagMapper extends Application_Model_Mappers_Abstract {

    /**
     * Tags database table class name
     *
     * @var string
     */
    protected $_dbTable = 'Tblog_Models_DbTable_Tag';

    /**
     * Tags model class name
     *
     * @var string
     */
    protected $_model   = 'Tblog_Models_Model_Tag';

    /**
     * Save tag to the system
     *
     * @param Tblog_Models_Model_Tag $tag
     * @return Tblog_Models_Model_Tag
     * @throws Exceptions_Tblog
     */
    public function save($tag) {
        if(!$tag instanceof $this->_model) {
            throw new Exceptions_TblogException('Cannot save post. Value passed to mapper should be Tblog_Models_Model_Tag instance');
        }

        $data = array(
            'name' => $tag->getName()
        );

        if($id = $tag->getId()) {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        } else {
            $tagId = $this->getDbTable()->insert($data);
            if($tagId) {
                $tag->setId($tagId);
            } else {
                throw new Exceptions_Tblog('Can not save post tag!');
            }
        }
        $tag->notifyObservers();
        return $tag;
    }

    /**
     * Find tag by name
     *
     * @param string $name
     * @return Tblog_Models_Model_Tag|null
     */
    public function findByName($name) {
        return $this->_findWhere($this->getDbTable()->getAdapter()->quoteInto('name = ?', $name));
    }

    /**
     * Find tags for specific post according to post's id
     *
     * @param integer $id
     * @return array|null
     */
    public function findByPostId($id) {
        $entries = array();
        $where   =  $this->getDbTable()->getAdapter()->quoteInto('pht.post_id=?', $id);
        $select  = $this->getDbTable()->getAdapter()->select()
            ->from(array('pht' => 'plugin_tblog_post_has_tag'))
            ->join(array('t' => 'plugin_tblog_tag'), 'pht.tag_id=t.id')
            ->where($where);
        $tags = $this->getDbTable()->getAdapter()->fetchAll($select);
        if(!is_array($tags) || empty($tags)) {
            return null;
        }
        foreach($tags as $tagData) {
            $entries[] = new $this->_model($tagData);
        }
        return $entries;
    }

    /**
     * Delete tag from the system
     *
     * @param Tblog_Models_Model_Tag $tag
     * @return mixed
     */
    public function delete(Tblog_Models_Model_Tag $tag) {
        $where  = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $tag->getId());
        $result = $this->getDbTable()->delete($where);
        $tag->notifyObservers();
        return $result;
    }

}