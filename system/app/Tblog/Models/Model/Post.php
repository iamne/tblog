<?php
/**
 * Tblog post model
 *
 */
class Tblog_Models_Model_Post extends Application_Model_Models_Abstract {

    /**
     * Post title
     *
     * @var string
     */
    protected $_title       = '';

    /**
     * Post teaser
     *
     * @var string
     */
    protected $_teaser      = '';

    /**
     * Post content
     *
     * @var string
     */
    protected $_content     = '';

    /**
     * Date of post creation
     *
     * @var string
     */
    protected $_createdAt   = '';

    /**
     * Post update date
     *
     * @var string
     */
    protected $_updatedAt   = '';

    /**
     * Post related page id
     *
     * @var integer
     */
    protected $_pageId      = null;

    /**
     * Post tags
     *
     * @var array
     */
    protected $_tags        = array();

    /**
     * Post user (author) id
     *
     * @var integer
     */
    protected $_userId      = 0;

    /**
     * Post teaser image path
     *
     * @var string
     */
    protected $_teaserImage = '';

    /**
     * Set post content
     *
     * @param string $content
     * @return Tblog_Models_Model_Post $this
     */
    public function setContent($content) {
        $this->_content = $content;
        return $this;
    }

    /**
     * Get post content
     *
     * @return string
     */
    public function getContent() {
        return $this->_content;
    }

    /**
     * Set post creation date
     *
     * @param string $createdAt
     * @return Tblog_Models_Model_Post $this
     */
    public function setCreatedAt($createdAt) {
        $this->_createdAt = $createdAt;
        return $this;
    }

    /**
     * Get post creation date
     *
     * @return string
     */
    public function getCreatedAt() {
        return $this->_createdAt;
    }

    /**
     * Set post related page id
     *
     * @param integer $pageId
     * @return Tblog_Models_Model_Post $this
     */
    public function setPageId($pageId) {
        $this->_pageId = $pageId;
        return $this;
    }

    /**
     * Get post related page id
     *
     * @return integer
     */
    public function getPageId() {
        return $this->_pageId;
    }

    /**
     * Set post tags
     *
     * @param array $tags
     * @return Tblog_Models_Model_Post $this
     */
    public function setTags($tags) {
        $this->_tags = $tags;
        return $this;
    }

    /**
     * Get post tags
     *
     * @return array
     */
    public function getTags() {
        return (is_string($this->_tags)) ? explode(',', $this->_tags) : $this->_tags;
    }

    /**
     * Set post teaser
     *
     * @param string $teaser
     * @return Tblog_Models_Model_Post $this
     */
    public function setTeaser($teaser) {
        $this->_teaser = $teaser;
        return $this;
    }

    /**
     * Get post teaser
     *
     * @return string
     */
    public function getTeaser() {
        return $this->_teaser;
    }

    /**
     * Set post title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title) {
        $this->_title = $title;
        return $this;
    }

    /**
     * Get post title
     *
     * @return string
     */
    public function getTitle() {
        return $this->_title;
    }

    /**
     * Set post creation date
     *
     * @param integer $updatedAt
     * @return Tblog_Models_Model_Post $this
     */
    public function setUpdatedAt($updatedAt) {
        $this->_updatedAt = $updatedAt;
        return $this;
    }

    /**
     * Get post creation date
     *
     * @return string
     */
    public function getUpdatedAt() {
        return $this->_updatedAt;
    }

    /**
     * Set user (author) id
     *
     * @param integer $userId
     * @return Tblog_Models_Model_Post $this
     */
    public function setUserId($userId) {
        $this->_userId = $userId;
        return $this;
    }

    /**
     * Get user (author) id
     *
     * @return integer
     */
    public function getUserId() {
        return $this->_userId;
    }

    /**
     * Get instance of the page or null if page id is not set
     *
     * @return null|Application_Model_Models_Page
     */
    public function getPage() {
        return (($page = $this->_getPage()) !== null) ? $page : null;
    }

    /**
     * Set post teaser image
     *
     * @param string $teaserImage
     * @return Tblog_Models_Model_Post $this
     */
    public function setTeaserImage($teaserImage) {
        $this->_teaserImage = $teaserImage;
        return $this;
    }

    /**
     * Get post teaser image path
     *
     * @param bool $postOnly
     * @return null|string
     */
    public function getTeaserImage($postOnly = false) {
        if($postOnly) {
            return $this->_teaserImage;
        }
        if((($page = $this->_getPage()) !== null) && $page->getPreviewImage()) {
            return $page->getPreviewImage();
        }
        return '';
    }


    /**
     * Get post's url
     *
     * @return null|string
     */
    public function getUrl() {
        if(($page = $this->_getPage()) !== null) {
            return $page->getUrl();
        }
        return null;
    }

    public function toArray() {
        $postData         = parent::toArray();
        $postData['url']  = $this->getUrl();
        if(is_array($postData['tags'])) {
            $postData['tags'] = array_map(function($tag) { return ($tag instanceof Tblog_Models_Model_Tag) ? $tag->getName() : $tag; }, $postData['tags']);
        }
        return $postData;
    }

    /**
     * Internal method to find post related page
     *
     * @return Application_Model_Models_Page|null
     */
    private function _getPage() {
        if(!$this->_pageId) {
            return null;
        }
        $page = Application_Model_Mappers_PageMapper::getInstance()->find($this->_pageId);
        return (!$page instanceof Application_Model_Models_Page) ? null : $page;
    }
}