<?php

class Tblog_Models_Model_Tag extends Application_Model_Models_Abstract {

    /**
     * Tag name
     *
     * @var string
     */
    protected $_name = '';

    /**
     * Set tag name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    /**
     * Get tag name
     *
     * @return string
     */
    public function getName() {
        return $this->_name;
    }


}