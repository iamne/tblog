<?php

class Tblog_Tools_Tools {

    public static function uniqueTags(array $tags) {
        $uniqueTags = array();
        if(!empty($tags)) {
            foreach($tags as $tag) {
                if(!in_array($tag, $uniqueTags)) {
                    $uniqueTags[] = $tag;
                }
            }
        }
        return $uniqueTags;
    }

}