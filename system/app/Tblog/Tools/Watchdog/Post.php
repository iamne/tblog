<?php

class Tblog_Tools_Watchdog_Post extends Tools_System_GarbageCollector {

    /**
     * Post item
     *
     * @var Tblog_Models_Model_Post
     */
    private $_post          = null;

    private $_pageMapper    = null;

    private $_pageHelper    = null;

    private $_websiteHelper = null;

    /**
     * Seotoaster cache helper
     *
     * @var Helpers_Action_Cache
     */
    private $_cacheHelper   = null;

    public function __construct($params = array()) {
        parent::__construct($params);

        $this->_pageHelper    = Zend_Controller_Action_HelperBroker::getStaticHelper('page');
        $this->_pageMapper    = Application_Model_Mappers_PageMapper::getInstance();
        $this->_websiteHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('website');
        $this->_cacheHelper   = Zend_Controller_Action_HelperBroker::getStaticHelper('cache');
    }

    protected function _runOnDefault() {}

    protected function _runOnCreate() {
        // little overkill just to add a syntax sugar
        $this->_post = $this->_object;

        $postTitle   = $this->_post->getTitle();
        $tags        = $this->_post->getTags();

        // save page for post
        $template = Tblog_Models_Mapper_SettingsMapper::getInstance()->fetchValue('template');
        if(!$template) {
            $template = 'default';
        }

        if(($page = $this->_post->getPage()) === null) {
            $page = new Application_Model_Models_Page();
        }

        $page->setTemplateId($template)
            ->setParentId(Application_Model_Models_Page::IDCATEGORY_DEFAULT)
            ->setH1($postTitle)
            ->setNavName($postTitle)
            ->setHeaderTitle($postTitle)
            ->setUrl($this->_pageHelper->filterUrl($postTitle))
            ->setMetaKeywords((is_array($tags) && !empty($tags)) ? implode(', ', $tags) : '')
            ->setMetaDescription($this->_post->getTeaser())
            ->setShowInMenu(Application_Model_Models_Page::IN_NOMENU)
            ->setTargetedKeyPhrase($postTitle)
            ->setSystem(true)
            ->setLastUpdate(date(Tools_System_Tools::DATE_MYSQL))
            ->setExtraOptions(Tblog::OPTION_PAGE_DEFAULT, true)
            ->setTeaserText($this->_post->getTeaserImage());

        // processing post preview
        $postPageTeaserImage = $this->_post->getTeaserImage();
        $teaserImage         = $this->_post->getTeaserImage(true);
        if($teaserImage && ($postPageTeaserImage != $teaserImage)) {
            $teaserImagePath = $this->_websiteHelper->getPath() . $teaserImage;
            $teaserImage     = Tools_Page_Tools::processPagePreviewImage($page->getUrl(), $teaserImagePath);
            $page->setPreviewImage($teaserImage);
        }

        $page = $this->_pageMapper->save($page);

        // updating post
        $this->_post->setPageId($page->getId())
            ->setTeaserImage(($teaserImage) ? ($teaserImage) : null)
            ->removeObserver(new Tblog_Tools_Watchdog_Post);
        $this->_cleanPageCache();
        return Tblog_Models_Mapper_PostMapper::getInstance()->save($this->_post);
    }

    protected function _runOnDelete() {
        // little overkill just to add a syntax sugar
        $this->_post = $this->_object;
        $this->_cleanPageCache();
        return Application_Model_Mappers_PageMapper::getInstance()->delete($this->_post->getPage());
    }

    private function _cleanPageCache() {
        if(!$this->_post instanceof Tblog_Models_Model_Post) {
            return false;
        }
        $page = $this->_post->getPage();
        if(!$page instanceof Application_Model_Models_Page) {
            return false;
        }
        $this->_cacheHelper->clean(false, false, array('pageid_' . $page->getId()));
    }

}