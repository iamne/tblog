<?php

class Widgets_Tpost_Tpost extends Widgets_Abstract {

    /**
     * Prefix for the render methods
     *
     */
    const RENDERER_PREFIX       =  '_render';

    /**
     * Flag to indicate is this widget cacheable or not
     * @var bool
     */
    protected $_cacheable       = false;

    /**
     * Tblog posts mapper
     *
     * @var Tblog_Models_Mapper_PostMapper
     */
    protected $_mapper          = null;

    /**
     * Website helper instance
     *
     * @var Helpers_Action_Website
     */
    protected $_websiteHelper   = null;

    protected function _init() {

        $this->_view          = new Zend_View(array('scriptPath' => __DIR__ . '/views'));
        $this->_websiteHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('website');
        $this->_mapper        = Tblog_Models_Mapper_PostMapper::getInstance();

        //check the first option if it's integer we assume the page id is passed
        if(isset($this->_options[0]) && intval($this->_options[0])) {
            $this->_toasterOptions['id'] = array_shift($this->_options);
        }
    }

    protected function _load() {
        if(empty($this->_options)) {
            throw new Exceptions_TblogException('Not enough parameters passed!');
        }

        $option   = strtolower(array_shift($this->_options));
        $renderer = self::RENDERER_PREFIX . ucfirst($option);
        if(method_exists($this, $renderer)) {
            return $this->$renderer();
        }
        return $this->_renderOption($option);
    }

    /**
     * Common method to get post's option
     *
     * @param string $option
     * @return string
     */
    protected function _renderOption($option) {
        $getter = 'get' . ucfirst($option);

        try {
            $post = $this->_invokePost();
        } catch (Exceptions_TblogException $tbe) {
            return $tbe->getMessage();
        }

        if(!method_exists($post, $getter)) {
            return 'Tpost widget error: wrong option (' . $option . ') passed.';
        }
        return $post->$getter();
    }

    /**
     * Render post creation date in any date format
     *
     * @return string
     */
    protected function _renderDate() {
        $format = array_shift($this->_options);
        if(!$format) {
            $format = 'M, j Y H:m';
        }
        return $this->_translator->translate(date($format, strtotime($this->_invokePost()->getCreatedAt())));
    }

    /**
     * Render post teaser image url
     *
     * @todo optimize it!
     * @return null|string
     */
    protected function _renderTeaserImage() {
        $teaserImage = $this->_invokePost()->getTeaserImage();
        if(!$teaserImage) {
            return 'http://placehold.it/170x120';
        }
        return $this->_websiteHelper->getUrl() . $this->_websiteHelper->getPreview() . $teaserImage;
    }

    /**
     * Render edit link for the post
     *
     */
    protected function _renderEdit() {
        $post = $this->_invokePost();
        if(Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_PLUGINS)) {
            return '<a href="#edit/' . $post->getId() . '" class="tpost-edit-link" title="Edit: ' . $post->getTitle() . '">' . $this->_translator->translate('edit post') . '</a>';
        }
    }

    private function _invokePost() {
        if(!isset($this->_toasterOptions['id'])) {
            throw new Exceptions_TblogException('Tpost widget error: Can not determine page id.');
        }
        $page = Application_Model_Mappers_PageMapper::getInstance()->find($this->_toasterOptions['id']);
        if(!$page instanceof Application_Model_Models_Page) {
            throw new Exceptions_TblogException('Tpost widget error: Post page cannot be found');
        }
        if(!$page->getExtraOption(Tblog::OPTION_PAGE_DEFAULT)) {
            throw new Exceptions_TblogException('Tpost widget error: Page passed to the widget is not a post page');
        }
        $post = $this->_mapper->findByPageId($page->getId());
        if(!$post instanceof Tblog_Models_Model_Post) {
            throw new Exceptions_TblogException('Tpost widget error: Post item cannot be found');
        }
        return $post;
    }

}