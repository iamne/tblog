SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

SET NAMES utf8;

-- -----------------------------------------------------
-- Table `plugin_tblog_post`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `plugin_tblog_post`;

CREATE TABLE `plugin_tblog_post` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `teaser` text COLLATE utf8_unicode_ci,
  `teaser_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `page_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ind_created_at` (`created_at`),
  KEY `fk_post_page` (`page_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `plugin_tblog_post_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `plugin_tblog_post_ibfk_2` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Post entity';


-- -----------------------------------------------------
-- Table `plugin_tblog_tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `plugin_tblog_tag`;

CREATE TABLE `plugin_tblog_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ind_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tag entity';

-- -----------------------------------------------------
-- Table `plugin_tblog_post_has_tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `plugin_tblog_post_has_tag`;

CREATE TABLE `plugin_tblog_post_has_tag` (
  `post_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`post_id`,`tag_id`),
  KEY `fk_post_has_tag_tag` (`tag_id`),
  KEY `fk_post_has_tag_post` (`post_id`),
  CONSTRAINT `plugin_tblog_post_has_tag_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `plugin_tblog_tag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `plugin_tblog_post_has_tag_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `plugin_tblog_post` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- -----------------------------------------------------
-- Table `plugin_tblog_settings`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `plugin_tblog_settings`;

CREATE TABLE `plugin_tblog_settings` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- -----------------------------------------------------
-- Insert some initial values
-- -----------------------------------------------------
INSERT INTO `page_option` (`id`, `title`, `context`, `active`) VALUES ('option_tblog_index_page', 'Tblog index page', 'Tblog otions', '1');
INSERT INTO `page_option` (`id`, `title`, `context`, `active`) VALUES ('option_tblog_regular_page', 'Tblog regular page', 'Tblog otions', '1');
INSERT INTO `plugin_tblog_settings` (`name`, `value`) VALUES ('route', 'blog');
INSERT INTO `observers_queue` (`observable`, `observer`) VALUES ('Application_Model_Models_Page', 'Tblog_Tools_Watchdog_Page');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
