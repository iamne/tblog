SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

SET foreign_key_checks=0;

DROP TABLE IF EXISTS `plugin_tblog_post` ;
DROP TABLE IF EXISTS `plugin_tblog_tag` ;
DROP TABLE IF EXISTS `plugin_tblog_post_has_tag` ;
DROP TABLE IF EXISTS `plugin_tblog_settings` ;

DELETE FROM `page_option` WHERE `id` = 'option_tblog_index_page';
DELETE FROM `page_option` WHERE `id` = 'option_tblog_regular_page';
DELETE FROM `observers_queue` WHERE `observer` = 'Tblog_Tools_Watchdog_Page';

SET foreign_key_checks=1;