requirejs.config({
    paths: {
        'backbone': '../libs/backbone/backbone.min',
        'underscore': '../libs/underscore/underscore.min'
    },
    shim: {
        'backbone': {
            deps: ['underscore'],
            exports: 'Backbone'
        },
        'underscore' : {exports: '_'}
    }
});

$(function() {
    var moduleName = $('.tblog-app-module').data('module');
    require(['modules/' + moduleName + '/index'], function(Module) {
        Module.run();
    });
});