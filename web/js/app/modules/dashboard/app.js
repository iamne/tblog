angular.module('dashboard', []).
    config(['$routeProvider', function($routeProvider) {
        $routeProvider.
            when('/create', {templateUrl: '/plugins/tblog/web/js/dashboard/partials/create.partial.html', controller: PostsController}).
            otherwise({redirectTo: '/'});
    }]);