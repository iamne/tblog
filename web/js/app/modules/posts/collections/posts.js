define(['backbone', '../models/post'], function(Backbone, PostModel) {

    var Posts = Backbone.Collection.extend({
        model: PostModel,
        url: function() {
            var url = '/api/tblog/posts/';
            return url;
        }
    });

    return Posts;
});