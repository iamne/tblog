define(['backbone', './views/app', './models/post', './views/post', './collections/posts'], function(Backbone, AppView, PostModel, PostView, PostsCollection) {
    var PostsRouter = Backbone.Router.extend({

        app: null,
        posts: null,

        routes: {
            ''          : 'addEditAction',
            'new'       : 'addEditAction',
            'edit/:id'  : 'addEditAction',
            'list'      : 'listAction'
        },

        initialize: function() {
            this.app   = new AppView();

            this.posts = new PostsCollection();
            this.posts.on('reset', this.renderPosts, this);
            this.posts.on('remove', this.renderPosts, this);
        },

        addEditAction: function(id) {
            var self = this;
            var post = new PostModel();

            $('#mange-posts-holder').slideUp();

            if(!_.isUndefined(id)) {
                post.fetch({data: {id: id}}).done(function() {
                    self.app.setModel(post);
                });
            } else {
                self.app.setModel(post);
            }
        },

        listAction: function() {
            showSpinner();
            $('#mange-posts-holder').slideDown();
            this.posts.fetch({
                reset: true,
                success: function() {
                    hideSpinner();
                }
            })
        },

        renderPosts: function() {
            $('ul.thumbnails').empty();
            this.posts.each(function(postModel) {
                var postView = new PostView({model: postModel});
                $('ul.thumbnails').append(postView.render().$el)
            }, this);
        }
    });

    return {
        run: function() {
            window.appRouter = new PostsRouter();
            Backbone.history.start();
        }
    }
});