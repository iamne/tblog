define(['backbone'], function(Backbone){

    var PostModel = Backbone.Model.extend({
        urlRoot: '/api/tblog/posts/id/'
    });

    return PostModel;
});
