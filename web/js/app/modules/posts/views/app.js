define(['backbone'], function(Backbone) {

    var AppView = Backbone.View.extend({

        el: '.tblog-app-module',

        model: null,
        editor: null,
        websiteUrl: null,

        events: {
            'click #save-post': 'saveAction',
            'change [data-property]': 'setProperty',
            'change #tblog-pick-a-folder': 'loadMedia',
            'click .tblog-media': 'insertMedia',
            'click a.compose-toggle': 'toggleWindow'
        },

        initialize: function() {
            this.websiteUrl = this.$('#website_url').val();
            // init editor
            this._initEditor();
            // init uploader
            this._initUploader();
            // init accordion
            this._initAccordion();
            // init media folder list
            this._initMediaFolders();
        },

        _initEditor: function() {
            CKEDITOR.replace('frm-ta-tblog-content', {
                uiColor       : '#fafafa',
                language      : 'ru',
                removePlugins : 'elementspath',
                height        : '450px',
                toolbar       : [
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ], items: [ 'NumberedList', 'BulletedList'] },
                    { name: 'styles', items: [ 'Styles', 'Format' ] },
                    { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
                    { name: 'insert', items: [ 'Image', 'Table'] },
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] }
                ]
            });
            this.editor = CKEDITOR.instances['frm-ta-tblog-content'];
        },

        _initMediaFolders: function() {
            // get folders list for the dropdown
            var self = this;
            $.getJSON(self.websiteUrl + 'backend/backend_content/refreshfolders/', function(response) {
                if(response.responseText != null && typeof response.responseText === 'object') {
                    $.each(response.responseText, function(index, folder) {
                        self.$('#tblog-pick-a-folder').append($('<option />')
                            .val(folder)
                            .text(folder));
                    })
                }
            });
        },

        _initUploader: function() {
            var self               = this,
                websiteUrl         = self.websiteUrl,
                teaserImage        = self.$('#post-preview-image'),
                teaserImageSpinner = self.$('#teaser-upload-progress'),
                uploader           = new plupload.Uploader({
                    runtimes       : 'html5,flash,html4',
                    browse_button  : 'post-preview-image',
                    rename         : true,
                    max_file_size  : '5mb',
                    max_file_count : 1,
                    url            : websiteUrl + 'api/tblog/media/',
                    flash_swf_url  : websiteUrl + 'system/js/external/plupload/plupload.flash.swf'
                });
            uploader.init();

            uploader.bind('BeforeUpload', function(up) {
                up.settings.multipart_params = {
                    postId: self.model.get('id')
                }
            });

            uploader.bind('FilesAdded', function(up) {
                up.refresh();
                up.start();
                teaserImage.css({opacity: 0.2});
                teaserImageSpinner.show();
            });

            uploader.bind('FileUploaded', function(up, file, info) {
                var teaserPath = JSON.parse(info.response);
                $('#post-preview-image').attr({
                    src : websiteUrl + teaserPath
                });
                $('#teaser-image-path').val(teaserPath);
                teaserImage.css({opacity: 1});
                teaserImageSpinner.hide();
            });
        },

        _initAccordion: function() {
            $('#tblog-accordion').accordion({
                active     : false,
                collapsible: true,
                heightStyle: "content"
            });
        },

        setModel: function(model) {
            this.model = model;
            this.model.view = this;
            this.render();
        },

        setProperty: function(e) {
            var data     = {},
                property = this.$(e.currentTarget).data('property'),
                value    = this.$(e.currentTarget).val();
            if(property == 'tags') {
                value = _.map(value.split(','), $.trim);
            }
            data[property] = value;
            this.model.set(data);
        },

        toggleWindow: function(e) {
            var context = this.$(e.currentTarget).hasClass('compose-toggle') ? top : this;
            context.$('#' + this.$(e.currentTarget).data('windowId')).slideToggle();
        },

        // @todo change render to use underscore templates
        loadMedia: function(e) {
            var self         = this,
                thumbsHolder = self.$('.tablog-accordion-content-holder'),
                filesHolder  = self.$('.tablog-accordion-content-holder-files');
            $.ajax({
                type : 'get',
                url  : self.websiteUrl + 'api/tblog/media/folder/' + $(e.currentTarget).val()
            }).done(function(response) {
                thumbsHolder.empty();
                filesHolder.empty();
                if(response.hasOwnProperty('images')) {
                    $.each(response.images, function(key, path) {
                        thumbsHolder.append($('<img />').attr({
                            src    : self.websiteUrl + 'media/' + path.replace('/', '/product/'),
                            class  : 'tblog-media tblog-media-thumbnail img-polaroid',
                            width  : '70',
                            height : '70'
                        }));
                    });
                }
                if(response.hasOwnProperty('files')) {
                    $.each(response.files, function(key, path) {
                        filesHolder.append($('<a />').attr({
                            href   : 'javascript:;',
                            class  : 'tblog-media tblog-media-file btn btn-link btn-mini'
                        }).data('path', path).text(path.replace($(e.currentTarget).val() + '/', '')));
                    });
                }
            });
        },

        insertMedia: function(e) {
            this.editor.filter.allow('a[*]{*}(*)');

            if($(e.currentTarget).hasClass('tblog-media-thumbnail')) {
                var size = $(e.currentTarget).parent().data('size'),
                    mediaLink = '<a class="_lbox" href="' + $(e.currentTarget).attr('src').replace('/product/', '/original/') + '">' +
                        '<img src="'+ $(e.currentTarget).attr('src').replace('/product/', '/' + size + '/') + '" /></a>';
            }
            if($(e.currentTarget).hasClass('tblog-media-file')) {
                mediaLink = '<a href="' + websiteUrl + 'media/' + $(e.currentTarget).data('path') + '">' + $(e.currentTarget).text() + '</a>';
            }

            this.editor.insertHtml(mediaLink);
        },

        saveAction: function() {
            var self = this;
            // force save content
            self.model.set({content: self.editor.getData()});
            // save teaser image
            self.model.set({teaserImage:self.$('#teaser-image-path').val()});
            // save post
            showSpinner();
            self.model.save(null, {
                success: function(model, response, options) {
                    hideSpinner();

                    //change tmp path for the real one (teaser img)
                    self.$('#teaser-image-path').val(self.model.get('teaserImage'));

                    var timeOut = 1000;
                    showMessage(self.$('#frm-tblog-post').data('successMsg'), false, timeOut);
                    setTimeout(function() {
                        top.location.href = self.websiteUrl + response.url;
                    }, timeOut);
                },
                error: function() {
                    showMessage(self.$('#frm-tblog-post').data('errorMsg'), true);
                }
            });
        },

        render: function() {
            if(!this.model.isNew()) {
                this.$('#frm-inp-tblog-title').val(this.model.get('title'));
                this.$('#frm-ta-tblog-teaser').val(this.model.get('teaser'));
                this.editor.setData(this.model.get('content'));
                //this.editor.insertHtml();

                var tags = this.model.get('tags');
                if(!_.isNull(tags)) {
                    this.$('#frm-inp-tblog-tags').val((!_.isUndefined(tags)) ? tags.join(', ') : '');
                }

                var teaserImage = this.model.get('teaserImage');
                if(teaserImage) {
                    this.$('#post-preview-image').attr({src: this.websiteUrl + 'previews/' + teaserImage});
                }

            } else {
                this.$('#frm-inp-tblog-title').val('');
                this.$('#frm-ta-tblog-teaser').val('');
                this.editor.setData('');
                this.$('#frm-inp-tblog-tags').val('');
                this.$('#post-preview-image').attr({src: 'http://placehold.it/170x120'});
            }
            return this;
        }

    });

    return AppView;
});