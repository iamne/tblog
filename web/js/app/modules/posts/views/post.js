define(['backbone'], function(Backbone) {

    var PostView = Backbone.View.extend({
        tagName  : 'li',
        className: 'span2',
        template : _.template($('#post-item-template').text()),
        events   : {
            'click .btn-danger': 'deleteAction'
        },
        initialize: function() {
            this.model.view = this;
        },
        deleteAction: function(e) {
            var confirm = $('#mange-posts-holder').data('deleteConfirm');
            var self    = this;
            showConfirm(confirm, function() {
                showSpinner();
                self.model.destroy({
                    wait: true,
                    success: function() {
                        hideSpinner();
                    },
                    error: function(model, xhr, options) {
                        showMessage(xhr.responseText);
                    }
                })
            });
        },
        render: function(){
            $(this.el).html(this.template({post: this.model.toJSON()}));
            return this;
        }
    });
    return PostView;
});