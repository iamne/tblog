$(function() {

    var websiteUrl = $('#website_url').val(),
        thumbsHolder = $('.tablog-accordion-content-holder'),
        filesHolder  = $('.tablog-accordion-content-holder-files');

    //initialize ckeditor
    CKEDITOR.replace('frm-ta-tblog-content', {
        uiColor       : '#fafafa',
        language      : 'ru',
        removePlugins : 'elementspath',
        height        : '450px',
        toolbar       : [
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ], items: [ 'NumberedList', 'BulletedList'] },
            { name: 'styles', items: [ 'Styles', 'Format' ] },
            { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
            { name: 'insert', items: [ 'Image', 'Table'] },
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] }
        ]
    });

    // initialize right-side accordion
    $('#tblog-accordion').accordion({
        active     : false,
        collapsible: true,
        heightStyle: "content"
    });

    // init image uploader
    var teaserImage        = $('#post-preview-image');
    var teaserImageSpinner = $('#teaser-upload-progress');

    var uploader = new plupload.Uploader({
        runtimes       : 'html5,flash,html4',
        browse_button  : 'post-preview-image',
        rename         : true,
        max_file_size  : '5mb',
        max_file_count : 1,
        url            : websiteUrl + 'api/tblog/media/',
        flash_swf_url  : websiteUrl + 'system/js/external/plupload/plupload.flash.swf'
    });
    uploader.init();

    uploader.bind('BeforeUpload', function(up) {
        up.settings.multipart_params = {
            postId: null
        }
    });

    uploader.bind('FilesAdded', function(up) {
        up.refresh();
        up.start();
        teaserImage.css({opacity: 0.2});
        teaserImageSpinner.show();
    });

    uploader.bind('FileUploaded', function(up, file, info) {
        var teaserPath = JSON.parse(info.response);
        $('#post-preview-image').attr({
            src : $('#website_url').val() + teaserPath
        });
        $('#teaser-image-path').val(teaserPath);
        teaserImage.css({opacity: 1});
        teaserImageSpinner.hide();
    });

    var composeBox = $('#tblog-newpost-compose');

    // init compose box according to its last state
    if($.cookie('boxVisible') == 1 && self == top) {
        composeBox.show();
    }

    // toggle compose box
    $(document).on('click', 'a.compose-toggle', function() {
        composeBox.slideToggle(function() {
            $.cookie('boxVisible', (composeBox.is(":visible") === true) ? 1 : 0);
        });
    });

    $(".tblog-popup").fancybox({
        maxWidth	: 1000,
        maxHeight	: 600,
        fitToView	: true,
        width		: '100%',
        height		: '100%',
        autoSize	: false,
        closeClick	: false,
        openEffect	: 'none',
        closeEffect	: 'none'
    });

    // get folders list for the dropdown
    $.getJSON(websiteUrl + 'backend/backend_content/refreshfolders/', function(response) {
        if(response.responseText != null && typeof response.responseText === 'object') {
            $.each(response.responseText, function(index, folder) {
                $('#tblog-pick-a-folder').append($('<option />')
                    .val(folder)
                    .text(folder));
            })
        }
    });

    // inserting stuff into the editor
    $(document).on('click', '.tblog-thumbnail', function(e) {
        CKEDITOR.instances['frm-ta-tblog-content'].insertHtml('<strong>Hello</strong>');
    });

    $(document).on('change', '#tblog-pick-a-folder', function(e) {
        $.ajax({
            type : 'get',
            url  : websiteUrl + 'api/tblog/media/folder/' + $(e.currentTarget).val()
        }).done(function(response) {
            thumbsHolder.empty();
            filesHolder.empty();
            if(response.hasOwnProperty('images')) {
                $.each(response.images, function(key, path) {
                    thumbsHolder.append($('<img />').attr({
                        src    : websiteUrl + 'media/' + path.replace('/', '/product/'),
                        class  : 'tblog-media tblog-media-thumbnail img-polaroid',
                        width  : '70',
                        height : '70'
                    }));
                });
            }
            if(response.hasOwnProperty('files')) {
                $.each(response.files, function(key, path) {
                    filesHolder.append($('<a />').attr({
                        href   : 'javascript:;',
                        class  : 'tblog-media tblog-media-file btn btn-link btn-mini'
                    }).data('path', path).text(path.replace($(e.currentTarget).val() + '/', '')));
                });
            }
        });
    });

    $(document).on('click', '.tblog-media', function(e) {
        CKEDITOR.instances['frm-ta-tblog-content'].filter.allow('a[*]{*}(*)');
        if($(e.currentTarget).hasClass('tblog-media-thumbnail')) {
            var size = $(e.currentTarget).parent().data('size'),
                mediaLink = '<a class="_lbox" href="' + $(e.currentTarget).attr('src').replace('/product/', '/original/') + '">' +
                '<img src="'+ $(e.currentTarget).attr('src').replace('/product/', '/' + size + '/') + '" /></a>';
        }
        if($(e.currentTarget).hasClass('tblog-media-file')) {
            mediaLink = '<a href="' + websiteUrl + 'media/' + $(e.currentTarget).data('path') + '">' + $(e.currentTarget).text() + '</a>';
        }

        CKEDITOR.instances['frm-ta-tblog-content'].insertHtml(mediaLink);
    });

    // create post button handler
    $(document).on('submit', '#frm-tblog-post', function(e) {
        e.preventDefault();
        var tags = $(e.currentTarget).find('#frm-inp-tblog-tags').val(),
            data = {
                title       : $.trim($(e.currentTarget).find('#frm-inp-tblog-title').val()),
                teaser      : $.trim($(e.currentTarget).find('#frm-ta-tblog-teaser').val()),
                teaserImage : $(e.currentTarget).find('#teaser-image-path').val(),
                content     : $.trim(CKEDITOR.instances['frm-ta-tblog-content'].getData()),
                tags        : tags.length ? $.map(tags.split(','), function(tag) { return $.trim(tag);}) : null
            };

        $.ajax({
            type       : 'post',
            url        : websiteUrl + 'api/tblog/posts/',
            data       : JSON.stringify(data),
            beforeSend : showSpinner
        }).done(function(response) {
            hideSpinner();
            top.location.href = response.url;
        });

        console.log(data);

    });

});